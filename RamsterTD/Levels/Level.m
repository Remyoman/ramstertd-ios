//
//  Level.m
//  RamsterTD
//
//  Created by Remy Baratte on 03/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "Level.h"
#import "Waypoint.h"
#import "RamsterData.h"

@implementation Level

-(instancetype)init
{
    if(self = [super init])
    {
        _waypoints = [NSMutableArray array];
        _creepPath = CGPathCreateMutable();
        _pathLine = [[SKShapeNode alloc] init];
        [_pathLine setStrokeColor:PATH_COLOR];
        [[RamsterData sharedInstance] setCurrentLevel:self];
    }
    return self;
}

-(void)initWaypointsForLevel:(int)level
{
    NSArray *waypointLocations = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Waypoints%.2d", level] ofType:@"plist"]];
    CGPathMoveToPoint(_creepPath, NULL, [[waypointLocations[0] objectForKey:@"x"] intValue], [[waypointLocations[0] objectForKey:@"y"] intValue]);
    for(int i = 1; i < waypointLocations.count; i++)
    {
        int x = [[waypointLocations[i] objectForKey:@"x"] intValue];
        int y = [[waypointLocations[i] objectForKey:@"y"] intValue];
        [_waypoints addObject:[[Waypoint alloc] initWithCoordinatesX:x andY:y]];
        NSLog(@"Waypoint: %d x: %d y: %d", i, x, y);
        CGPathAddLineToPoint(_creepPath, NULL, x, y);
    }
    _pathLine.path = _creepPath;
}

-(void)dealloc
{
    CGPathRelease(_creepPath);
}

@end
