//
//  Level.h
//  RamsterTD
//
//  Created by Remy Baratte on 03/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RamsterHeader.h"

@interface Level : SKSpriteNode
{
}

@property (nonatomic, strong) NSMutableArray *waypoints;
@property (nonatomic, assign) CGMutablePathRef creepPath;
@property (nonatomic, strong) SKShapeNode *pathLine;

-(instancetype)init;
-(void)initWaypointsForLevel:(int)level;

@end
