//
//  Waypoint.m
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "Waypoint.h"

@implementation Waypoint

-(instancetype)initWithCoordinatesX:(int)x andY:(int)y
{
    if(self = [super init])
    {
        _location = CGPointMake(x, y);
    }
    return self;
}

-(instancetype)initWithPoint:(CGPoint)pointLocation
{
    if(self = [super init])
    {
        _location = pointLocation;
    }
    return self;
}

@end
