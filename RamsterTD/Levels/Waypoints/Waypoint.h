//
//  Waypoint.h
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Waypoint : NSObject
{
}

@property (nonatomic, readonly, assign) CGPoint location;

-(instancetype)initWithCoordinatesX:(int)x andY:(int)y;
-(instancetype)initWithPoint:(CGPoint)pointLocation;

@end
