//
//  MyScene.m
//  RamsterTD
//
//  Created by Remy Baratte on 9/24/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "MainMenuScene.h"
#import "RamsterHeader.h"
#import "GameScene.h"

@implementation MainMenuScene

-(instancetype)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        /* Initialize the background and add it to the view. 
         * Secondly, add the play button to start the game with. */
        background = [SKSpriteNode spriteNodeWithImageNamed:@"MainMenu"];
        playButton = [SKSpriteNode spriteNodeWithImageNamed:@"Continue"];
        [background setAnchorPoint:CGPointZero];
        playButton.position = CGPointMake(240, 160);
        [self addChild:background];
        [self addChild:playButton];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* Called when a touch begins */
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        if([playButton containsPoint:location])
        {
            [self runAction:[SKAction runBlock:^
                             {
                                 SKTransition *startGame = [SKTransition doorwayWithDuration:TRANSITION_TIME];
                                 SKScene *gameScene = [[GameScene alloc] initWithSize:self.size];
                                 [self.view presentScene:gameScene transition:startGame];
                             }]];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime
{
    /* Called before each frame is rendered */
}

@end
