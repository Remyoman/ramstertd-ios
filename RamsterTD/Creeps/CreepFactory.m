//
//  CreepFactory.m
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "CreepFactory.h"
#import "NormalCreep.h"
#import "RamsterData.h"

@implementation CreepFactory

+(Creep *)createCreep:(CreepType)creepType
{
    Creep *creep = nil;
    switch (creepType)
    {
        case Normal:
            creep = [[NormalCreep alloc] init];
            break;
        case Fast:
            break;
        case Boss:
            break;
        default:
            creep = [[NormalCreep alloc] init];
            break;
    }
    [[[RamsterData sharedInstance] creeps] addObject:creep];
    return creep;
}

@end
