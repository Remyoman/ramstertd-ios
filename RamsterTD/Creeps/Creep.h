//
//  Creep.h
//  RamsterTD
//
//  Created by Remy Baratte on 03/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Creep : SKSpriteNode

-(instancetype)init;
-(void)startMoving;

@end
