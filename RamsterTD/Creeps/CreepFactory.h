//
//  CreepFactory.h
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RamsterHeader.h"
#import "Creep.h"

@interface CreepFactory : NSObject

+(Creep *)createCreep:(CreepType)creepType;

@end
