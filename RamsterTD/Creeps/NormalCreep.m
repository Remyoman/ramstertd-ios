//
//  NormalCreep.m
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "NormalCreep.h"
#import "RamsterData.h"

@implementation NormalCreep

-(instancetype)init
{
    if(self = [super init])
    {
        self = [self initWithImageNamed:@"NormalCreep"];
    }
    return self;
}

-(void)startMoving
{
    SKAction *moveAlongPath = [SKAction followPath:[[[RamsterData sharedInstance] currentLevel] creepPath] asOffset:NO orientToPath:YES duration:PATH_TIME];
    [self runAction:moveAlongPath completion:^{
        NSLog(@"Creep done following path.");
    }];
}

@end
