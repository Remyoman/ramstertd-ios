//
//  RamsterHeader.h
//  RamsterTD
//
//  Created by Remy Baratte on 10/5/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//
#ifndef RamsterTD_RamsterHeader_h
#define RamsterTD_RamsterHeader_h

typedef NS_ENUM(NSInteger, CreepType)
{
    Normal,
    Fast,
    Boss
};

typedef NS_ENUM(NSInteger, TowerType)
{
    Standard,
    Fire,
    Ice,
    Poison
};

#define BACKGROUND_COLOR [SKColor colorWithRed:0.06 green:0.16 blue:0.28 alpha:1.0]
#define PATH_COLOR  [SKColor colorWithRed:0.02 green:0.06 blue:0.11 alpha:1.0]

#define TRANSITION_TIME 1.0
#define PATH_TIME       20.0

#define LEVEL_01    01

#endif
