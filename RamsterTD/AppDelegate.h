//
//  AppDelegate.h
//  RamsterTD
//
//  Created by Remy Baratte on 9/24/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
