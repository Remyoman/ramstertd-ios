//
//  GameScene.m
//  RamsterTD
//
//  Created by Remy Baratte on 10/4/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "RamsterHeader.h"
#import "RamsterData.h"
#import "CreepFactory.h"
#import "TowerFactory.h"
#import "GameScene.h"

@implementation GameScene

-(instancetype)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        /* Setup your scene here */
        self.backgroundColor = BACKGROUND_COLOR;
        [[[Level alloc] init] initWaypointsForLevel:LEVEL_01];
        //Code for adding the path to the view and setting up a creep to follow it.
        [self addChild:[[[RamsterData sharedInstance] currentLevel] pathLine]];
        Creep *dummy = [CreepFactory createCreep:Normal];
        [self addChild:dummy];
        [dummy startMoving];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITouch *touch in touches)
    {
        Tower *tower = [TowerFactory createTower:Standard atPosition:[touch locationInNode:self]];
        [self addChild:tower];
    }
}

-(void)update:(CFTimeInterval)currentTime
{
    /* Called before each frame is rendered */
}

@end
