//
//  RamsterData.h
//  RamsterTD
//
//  Created by Remy Baratte on 02/01/14.
//  Copyright (c) 2014 Remy Baratte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"

@interface RamsterData : NSObject <NSCoding>
{
}

@property (nonatomic, strong) Level *currentLevel;
@property (nonatomic, strong) NSMutableArray *towers;
@property (nonatomic, strong) NSMutableArray *creeps;

+(instancetype)sharedInstance;
-(instancetype)initWithCoder:(NSCoder *)decoder;
-(void)encodeWithCoder:(NSCoder *)coder;

@end
