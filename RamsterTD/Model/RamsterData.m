//
//  RamsterData.m
//  RamsterTD
//
//  Created by Remy Baratte on 02/01/14.
//  Copyright (c) 2014 Remy Baratte. All rights reserved.
//

#import "RamsterData.h"
#import "RamsterHeader.h"

@implementation RamsterData

+(instancetype)sharedInstance
{
    static RamsterData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RamsterData alloc] init];
    });
    return sharedInstance;
}

-(instancetype)initWithCoder:(NSCoder *)decoder
{
    RamsterData *instance = [RamsterData sharedInstance];
    return instance;
}

-(void)encodeWithCoder:(NSCoder *)coder
{
    
}

@end
