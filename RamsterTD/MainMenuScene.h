//
//  MyScene.h
//  RamsterTD
//

//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MainMenuScene : SKScene
{
    SKSpriteNode *background;
    SKSpriteNode *playButton;
}

@end
