//
//  NormalTower.m
//  RamsterTD
//
//  Created by Remy Baratte on 17/12/13.
//  Copyright (c) 2013 Remy Baratte. All rights reserved.
//

#import "NormalTower.h"

@implementation NormalTower

-(instancetype)init
{
    if(self = [super init])
    {
        self = [self initWithImageNamed:@"NormalTower"];
        self.position = CGPointZero;
    }
    return self;
}

-(instancetype)initWithLocation:(CGPoint)location
{
    self = [self init];
    self.position = location;
    return self;
}

@end
