//
//  TowerFactory.h
//  RamsterTD
//
//  Created by Remy Baratte on 03/01/14.
//  Copyright (c) 2014 Remy Baratte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RamsterHeader.h"
#import "Tower.h"

@interface TowerFactory : NSObject

+(Tower *)createTower:(TowerType)towerType atPosition:(CGPoint)position;

@end
