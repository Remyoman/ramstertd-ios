//
//  TowerFactory.m
//  RamsterTD
//
//  Created by Remy Baratte on 03/01/14.
//  Copyright (c) 2014 Remy Baratte. All rights reserved.
//

#import "TowerFactory.h"
#import "NormalTower.h"
#import "RamsterData.h"

@implementation TowerFactory

+(Tower *)createTower:(TowerType)towerType atPosition:(CGPoint)position
{
    Tower *tower = nil;
    switch(towerType)
    {
        case Standard:
            tower = [[NormalTower alloc] initWithLocation:position];
            break;
        case Fire:
            break;
        case Ice:
            break;
        case Poison:
            break;
        default:
            tower = [[NormalTower alloc] init];
            break;
    }
    [[[RamsterData sharedInstance] towers] addObject:tower];
    return tower;
}

@end
